import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { infoPage } from "../info/info";
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  state: string;
  stappen: number;
  start: boolean;

  constructor(public navCtrl: NavController,private storage: Storage) {
      this.state="uit";

      
      storage.get('aantal').then((val) => {
        if(val==null)
          {
              this.stappen=0;
                // set a key/value
              storage.set('aantal', 0);
          }
          else
          {
              this.stappen=val;
          }
        
      });
      
  }

  goInfo()
  {
      this.navCtrl.push(infoPage);
  }

  toggle()
  {
    if(this.start)
      {
          this.state="aan";

      }
    else
      {
        this.state="uit";
        // set a key/value
        this.storage.set('aantal', 0);
      }
     
  }
}
